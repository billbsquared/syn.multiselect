/**
 * @projectName: multiselect
 * @version: 1.0.0
 * @author: Bill Brady <bill@thevikingcoder.com>
 * @file: /src/multiselect.js
 * @date: 10/7/2015
 */

angular.module('$multiselect', [])
    .run(function ($templateCache) {
        var tpl = [
            '<div class="dropdown ms-container" ng-class="menuOpen ? \'open\' : null" hide-menu="menuOpen=false">'
            , '<button type="button" class="btn btn-sm btn-default dropdown-toggle" ng-click="menuOpen = !menuOpen">'
            , '{{ dropdownTitle }} <i class="fa fa-caret-down drop-down"></i>'
            , '</button>'
            , '<ul class="dropdown-menu ms-menu">'
            , '<li>'
            , '<button type="button" class="btn btn-sm btn-default" ng-click="selectAll()">'
            , '<i class="fa fa-fw fa-check"></i> All'
            , '</button>'
            , '<button type="button" class="btn btn-sm btn-default" ng-click="selectNone()">'
            , '<i class="fa fa-fw fa-check"></i> None'
            , '</button>'
            , '</li>'
            , '<li ng-repeat="option in options.options track by $index">'
            , '<button class="btn btn-sm min-ele" ng-class="option.checked ? btnS : btnD" ng-click="onSelect($index)">'
            , '<i class="fa fa-fw" ng-class="option.checked ? checked : checkOpen"></i> {{ option.label }}'
            , '</button>'
            , '</li>'
            , '</ul>'
            , '</div>'
        ].join("");

        $templateCache.put('multiselect.tpl', tpl);
    })
    .directive('msDropdown', function ($rootScope) {
        return {
            restrict: 'A',
            scope: {
                options: '='
                , optionId: '=?'
                , optionValues: '=?'
                , onSelect: '&'
                , onSelectAll: '&'
                , onSelectNone: '&'
                , dropdownTitle: '=?'
            },
            templateUrl: 'multiselect.tpl',
            replace: true,
            link: function ($scope) {

                var id = checkForKeyVal($scope.options, 'id', 'optionId'),
                    values = checkForKeyVal($scope.options, 'values', 'optionValues');

                $scope.dropdownTitle = $scope.dropdownTitle || 'Copy To';
                $scope.originalOptions = angular.copy($scope.options);
                $scope.options = optionsParsed();
                $scope.checkOpen = "fa-square-o";
                $scope.checked = "fa-check-square-o";
                $scope.btnS = "btn-success";
                $scope.btnD = "btn-default";

                $scope.onSelect = function (idx) {
                    var status = toggleCheck(idx) ? 'selected' : 'unselected',
                        oId = checkForKeyVal($scope.originalOptions, 'id', 'optionId'),
                        oValues = $scope.originalOptions[values],
                        msg = {};
                    msg[oId] = id;
                    msg[status] = oValues[idx];
                    _broadCast('onSelect', msg);
                };

                $scope.selectAll = function () {
                    toggleCheckAll(true);
                    _broadCast('onSelectAll', {id: $scope.options['id'], options: $scope.options});
                };

                $scope.selectNone = function () {
                    toggleCheckAll();
                    _broadCast('onSelectAll', {id: $scope.options['id'], options: $scope.options});
                };

                function toggleCheckAll(state) {
                    state = state || false;
                    for (var i = 0; i < $scope.options['options'].length; i++) {
                        $scope.options['options']['checked'] = state;
                    }
                }

                function toggleCheck(idx) {
                    var current = $scope.options['options'][idx]['checked'],
                        newVal = current ? false : true;
                    $scope.options['options'][idx]['checked'] = newVal;
                    return newVal;
                }

                function optionsParsed() {
                    var optNew = {};
                    id = $scope.options[id];
                    optNew['id'] = id;
                    optNew['options'] = [];
                    for (var i = 0; i < $scope.options[values].length; i++) {
                        var val = $scope.options[values][i],
                            opt = {
                                checked: false,
                                label: val,
                                value: val
                            };
                        optNew['options'].push(opt);
                    }
                    return optNew;
                }

                function _broadCast(name, data) {
                    $rootScope.$broadcast(name, data);
                }

                function checkForKeyVal(obj, key, scopeId) {
                    if ($scope[scopeId]) {
                        return $scope[scopeId];
                    }
                    if (obj.hasOwnProperty(key)) {
                        return obj[key];
                    }
                }
            }
        }
    })
    .directive('hideMenu', function ($document) {
        return {
            link: function ($scope, ele, attrs) {
                var scopeExp = attrs['hideMenu'],
                    onDocClick = function (e) {
                        var isChild = ele.find(e.target).length > 0;

                        if (!isChild) {
                            $scope.$apply(scopeExp);
                        }
                    };

                $document.on("click", onDocClick);

                ele.on("$destroy", function () {
                    $document.off("click", onDocClick);
                });
            }
        }
    });