# Instructions for installation
1. Copy the contents of the dist/ directory to your project
2. Include the module in your index.html
3. Add $multiselect as dependency to your app

## How to use the example
1. Copy the contents of the dist/ directory to the example/ directory
2. Run (as sudo su) npm i -g http-server
3. Navigate to the example/ directory through the terminal
4. Run 'hs' in your terminal
5. Open browser to http://localhost:8080/