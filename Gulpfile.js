var gulp = require("gulp");
var less = require("gulp-less");
var uglify = require("gulp-uglify");
var minifyCss = require("gulp-minify-css");
var rename = require("gulp-rename");
var ngAnnotate = require("gulp-ng-annotate");
var runSequence = require("run-sequence");

var source = "src",
    dest = "dist";

gulp.task('default', function() {
    runSequence('less', 'js');
});

gulp.task('less', function() {
    return gulp.src("src/*.less")
        .pipe(less())
        .pipe(minifyCss({keepBreaks: true}))
        .pipe(rename('multiselect.min.css'))
        .pipe(gulp.dest(dest));
});

gulp.task('js', function() {
    return gulp.src("src/*.js")
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(rename('multiselect.min.js'))
        .pipe(gulp.dest(dest));
});