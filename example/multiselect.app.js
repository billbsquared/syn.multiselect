angular.module('ms.app', ['$multiselect'])

    .constant('_', window._)

    .controller('AppCtrl', function ($rootScope, $scope) {

        var clients = [
            'client_1'
            , 'client_2'
            , 'client_3'
        ];

        $scope.options_1 = [
            {
                id: 'assetId',
                values: 'clients',
                assetId: 250,
                clients: angular.copy(clients),
                here: 'is',
                some: 'arbitrary',
                data: 'that',
                will: 'be ignored'
            },
            {
                id: 'assetId',
                values: 'clients',
                assetId: 251,
                clients: angular.copy(clients),
                here: 'is',
                some: 'arbitrary',
                data: 'that',
                will: 'be ignored'
            }
        ];

        $scope.options_2 = [
            {
                assetId: 250,
                clients: angular.copy(clients),
                here: 'is',
                some: 'arbitrary',
                data: 'that',
                will: 'be ignored'
            },
            {
                assetId: 251,
                clients: angular.copy(clients),
                here: 'is',
                some: 'arbitrary',
                data: 'that',
                will: 'be ignored'
            }
        ];

        var onSelectName = $scope.ms_onSelectName = 'onSelect_client',
            onSelectAll = $scope.ms_onSelectAllName = 'onSelectAll_client',
            onSelectNone = $scope.ms_onSelectNoneName = 'onSelectNone_client';

        $scope.ms_optionId = 'assetId';
        $scope.ms_optionValues = 'clients';

        $scope.$on(onSelectName, function(e, obj) {
            console.log(onSelectName);
            console.log(obj);
        });

        $scope.$on(onSelectAll, function(e, obj) {
            console.log(onSelectAll);
            console.log(obj);
        });

        $scope.$on(onSelectNone, function(e, obj) {
            console.log(onSelectNone);
            console.log(obj);
        });

    })

;